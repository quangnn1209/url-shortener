package com.biztexter.urlshortener.domain;

import com.biztexter.urlshortener.domain.exception.ShortenerException;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Created by Quang on 24/10/17.
 */
@Service
public class CoreService {

    final static Logger log = LoggerFactory.getLogger(CoreService.class);

    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int    BASE     = ALPHABET.length();

    private String encode(long num) {
        StringBuilder sb = new StringBuilder();
        while ( num > 0 ) {
            sb.append( ALPHABET.charAt((int) (num % BASE)) );
            num /= BASE;
        }
        return sb.reverse().toString();
    }

    public long decode(String str) {
        long num = 0;
        for ( int i = 0; i < str.length(); i++ )
            num = num * BASE + ALPHABET.indexOf(str.charAt(i));
        return num;
    }

    public String nextCode(Long seed){
        if(!NumberUtils.isDigits(String.valueOf(seed))){
            throw new ShortenerException("Invalid id", HttpStatus.SERVICE_UNAVAILABLE);
        }
        return encode(seed);
    }
}
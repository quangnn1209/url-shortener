package com.biztexter.urlshortener.domain;

import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.RedirectionModal;
import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.ShortenUrlId;
import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.ShortenUrlModal;
import com.biztexter.urlshortener.dao.ShortenDao;
import com.biztexter.urlshortener.domain.exception.UnknownUrlException;
import com.google.api.client.util.DateTime;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Quang on 24/10/17.
 */
@Service
public class ShortenerService {

    static Logger log = LoggerFactory.getLogger(ShortenerService.class);

    @Autowired
    private CoreService coreService;
    @Autowired
    private ShortenDao shortenDao;

    public ShortenUrlModal shorten(Long userId, String url) {
        // Check url in db
        ShortenUrlModal shortenUrl;
        shortenUrl = shortenDao.findByUrl(userId, url);

        if (shortenUrl == null) {
            shortenUrl = new ShortenUrlModal();
            shortenUrl.setUrl(url);
            shortenUrl.setCreationDate(new DateTime(new Date()));
            shortenUrl.setHits(0L);

            ShortenUrlId shortenUrlId = shortenDao.save(userId, shortenUrl); // Saving new object
            shortenUrl.setId(shortenUrlId.getId());
            shortenUrl.setCode(coreService.nextCode(shortenUrl.getId())); // Convert id to code
            shortenDao.save(userId, shortenUrl); // Update code
        }

        return shortenUrl;
    }

    public String getRedirectionUrl(Long userId, String code, Map<String, String> requestInfo) {
        ShortenUrlModal shortenUrl = shortenDao.findByCode(userId, code);

        if(shortenUrl == null)
            throw new UnknownUrlException();
        shortenUrl.setHits(shortenUrl.getHits() + 1);
        shortenDao.save(userId, shortenUrl); // Update hit

        RedirectionModal redirectionModal = new RedirectionModal();
        redirectionModal.setShortenUrlId(shortenUrl.getId());
        redirectionModal.setUserAgent(requestInfo.get("userAgent"));
        redirectionModal.setSourceHost(requestInfo.get("sourceHost"));
        redirectionModal.setRedirectDate(new DateTime(new Date()));
        redirectionModal.setCode(shortenUrl.getCode());
        shortenDao.saveDirection(userId, redirectionModal);

        Calendar now = Calendar.getInstance();

        long timeDiff = now.getTimeInMillis() - shortenUrl.getCreationDate().getValue();

        String redirectUrl = shortenUrl.getUrl();
        if(timeDiff > 7 * 24 * 60 * 60 * 1000){
            redirectUrl = "https://www.google.com/search?q=" + redirectUrl;
        }

        log.info("Redirecting code["+code+"] url["+redirectUrl+"]");
        return redirectUrl;
    }

    public ShortenUrlModal getStats(Long userId, String code) {
        return shortenDao.findByCode(userId, code);
    }

    public Long getHit(Long userId, String code) {
        try {
            ShortenUrlModal shortenUrlModal = shortenDao.findByCode(userId, code);
            if(shortenUrlModal != null){
                return shortenUrlModal.getHits();
            }
        } catch (Exception e){
            log.info("[ERROR] Can not get hit of ["+code+"] Reason:" + (e instanceof UnknownUrlException ? ((UnknownUrlException) e).getKey() : ExceptionUtils.getStackTrace(e)));
        }
        return 0L;
    }
}
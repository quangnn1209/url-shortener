package com.biztexter.urlshortener.domain.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by Quang on 01/11/17.
 */
public class UnknownUrlException extends ShortenerException {

    public UnknownUrlException() {
        super("UNKNOWN_URL", HttpStatus.NOT_FOUND);
    }

}

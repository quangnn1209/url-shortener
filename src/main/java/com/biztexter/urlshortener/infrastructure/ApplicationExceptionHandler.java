package com.biztexter.urlshortener.infrastructure;

import com.biztexter.urlshortener.domain.exception.ErrorInfo;
import com.biztexter.urlshortener.domain.exception.ShortenerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Quang on 02/11/17.
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    @ExceptionHandler(ShortenerException.class)
    @ResponseBody
    ErrorInfo handleRestException(HttpServletResponse response, ShortenerException ex) {
        LOG.error("Returning [" + ex.getHttpErrorCode() + "] " + ex.getMessage());
        response.setStatus(ex.getHttpErrorCode().value());
        return new ErrorInfo(ex.getMessage(), ex.getHttpErrorCode(), ex.getKey());
    }
}
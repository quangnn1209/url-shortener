package com.biztexter.urlshortener.infrastructure.slack;

public enum SlackChannel {
    Exception("https://hooks.slack.com/services/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");

    private String channel;

    SlackChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }
}

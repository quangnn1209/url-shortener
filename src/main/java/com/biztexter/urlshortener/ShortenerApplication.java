package com.biztexter.urlshortener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * Created by Quang on 24/10/17.
 */
@SpringBootApplication
public class ShortenerApplication{
    private static final Logger logger = LoggerFactory.getLogger(ShortenerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ShortenerApplication.class, args);
    }
}

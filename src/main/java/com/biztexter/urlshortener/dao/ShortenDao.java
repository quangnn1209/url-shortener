package com.biztexter.urlshortener.dao;

import com.biztexter.cloudmessaging.client.CloudMessaging;
import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.RedirectionModal;
import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.ShortenUrlId;
import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.ShortenUrlModal;
import com.biztexter.urlshortener.domain.exception.ShortenerException;
import com.biztexter.urlshortener.domain.exception.UnknownUrlException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by quang on 17/10/12.
 */
@Component
public class ShortenDao {
    static Logger log = LoggerFactory.getLogger(ShortenDao.class);

    public ShortenUrlId save(Long userId, ShortenUrlModal shortenUrl){
        try {
            ShortenUrlId shortenUrlId = CloudMessaging.client(userId).saveShortenUrl(shortenUrl);
            if(shortenUrlId == null){
                throw new ShortenerException("Can not save shorten url", HttpStatus.SERVICE_UNAVAILABLE);
            }

            return shortenUrlId;
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ShortenerException("Can not save shorten url", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public List<ShortenUrlModal> listShortenUrls(Long userId) {
        try {
            return CloudMessaging.client(userId).findAllShortenUrl();
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ShortenerException("Can not get all shorten urls", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public List<ShortenUrlModal> findByUrlPatten(Long userId, String pattern, int batch) {
        try {
            return CloudMessaging.client(userId).findByUrlPattern(pattern, batch);
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ShortenerException("Can not get shorten urls by patten", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public ShortenUrlModal findById(Long userId, Long id) {
        try {
            ShortenUrlModal shortenUrl = CloudMessaging.client(userId).findById(id);
            if (shortenUrl == null)
                throw new UnknownUrlException();

            return shortenUrl;
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ShortenerException("Can not get shorten url by id", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public ShortenUrlModal findByCode(Long userId, String code) {
        try {
            ShortenUrlModal shortenUrl = CloudMessaging.client(userId).findByCode(code);
            if (shortenUrl == null)
                throw new UnknownUrlException();

            return shortenUrl;
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ShortenerException("Can not get shorten url by code", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public ShortenUrlModal findByUrl(Long userId, String url) {
        try {
            return CloudMessaging.client(userId).findByUrl(url);
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    public void saveDirection(Long userId, RedirectionModal redirectionModal){
        try {
            CloudMessaging.client(userId).saveRedirection(redirectionModal);
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }
}
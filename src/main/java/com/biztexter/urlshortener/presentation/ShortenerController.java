package com.biztexter.urlshortener.presentation;

import com.biztexter.urlshortener.application.ShortenerApplicationService;
import com.biztexter.urlshortener.application.representation.*;
import com.biztexter.urlshortener.domain.exception.ShortenerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Quang on 31/10/17.
 */
@RestController
public class ShortenerController {
    final static Logger log = LoggerFactory.getLogger(ShortenerController.class);
    private final static Long ADMIN = 66L;

    @Autowired
    private ShortenerApplicationService shortenerApplicationService;

    @RequestMapping(path = "/", method = RequestMethod.POST)
    @ResponseBody
    public ShortenResponse shorten(@RequestParam("key") String key, @RequestBody ShortenRequest request) {
        shortenerApplicationService.validateKey(key);
        if(StringUtils.isEmpty(request.getLongUrl())){
            throw new ShortenerException("Url is required", HttpStatus.BAD_REQUEST);
        }
        return shortenerApplicationService.shorten(ADMIN, request.getLongUrl());
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String root() {
        return "Equiitext's shortener service";
    }

    @RequestMapping(path = "/.well-known/pki-validation/godaddy.html", method = RequestMethod.GET)
    public String sslValidation() {
        return "82inebaj53n8dlaspa3a9usif6";
    }

    @RequestMapping(path = "/stat", method = RequestMethod.GET)
    public ShortenStats getStats(@RequestParam("key") String key, @RequestParam("shortUrl") String shortUrl) {
        shortenerApplicationService.validateKey(key);
        if(StringUtils.isEmpty(shortUrl)){
            throw new ShortenerException("Url is required", HttpStatus.BAD_REQUEST);
        }
        return shortenerApplicationService.getStats(ADMIN, shortUrl);
    }

    @RequestMapping(path = "/stat", method = RequestMethod.POST)
    public StatsBatchResponse getHit(@RequestParam("key") String key, @RequestBody StatsBatchRequest batch) {
        shortenerApplicationService.validateKey(key);
        if(CollectionUtils.isEmpty(batch.getShortUrls())){
            throw new ShortenerException("Urls are required", HttpStatus.BAD_REQUEST);
        }
        log.info("Getting hit by batch [" + batch.getShortUrls().size() +"] links");
        return shortenerApplicationService.getHit(ADMIN, batch.getShortUrls());
    }

    @RequestMapping(path = "/download", method = RequestMethod.POST)
    public DownloadStatBatch download(@RequestParam("key") String key, @RequestBody DownloadStatBatch batch) {
        shortenerApplicationService.validateKey(key);
        if(CollectionUtils.isEmpty(batch.getBatch())){
            throw new ShortenerException("Urls are required", HttpStatus.BAD_REQUEST);
        }
        log.info("Downloading stat by batch [" + batch.getBatch().size() +"] links");
        return shortenerApplicationService.download(ADMIN, batch);
    }

    @RequestMapping(path = "/{code}", method = RequestMethod.GET)
    public void redirect(@PathVariable String code, HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(shortenerApplicationService.getRedirectionUrl(ADMIN, code, getSourceInfoMap(request)));
    }

    private Map<String, String> getSourceInfoMap(HttpServletRequest request) {
        Map<String, String> sourceInfo = new HashMap<>();
        sourceInfo.put("userAgent", request.getHeader("User-Agent"));
        sourceInfo.put("sourceHost",request.getHeader("Host"));
        sourceInfo.put("sourceRequestDate", request.getHeader("Date"));
        return sourceInfo;
    }
}

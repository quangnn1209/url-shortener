package com.biztexter.urlshortener.application;

import com.biztexter.cloudmessaging.console.endpoints.client.cloudMessagingGoogleAppEngine.model.ShortenUrlModal;
import com.biztexter.urlshortener.application.representation.*;
import com.biztexter.urlshortener.domain.ShortenerService;
import com.biztexter.urlshortener.domain.exception.ShortenerException;
import com.biztexter.urlshortener.infrastructure.slack.SlackChannel;
import com.biztexter.urlshortener.infrastructure.slack.SlackHelper;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Stream;

/**
 * Created by Quang on 24/10/17.
 */
@Service
public class ShortenerApplicationService {
    private final static Logger log = LoggerFactory.getLogger(ShortenerApplicationService.class);
    private final static Map<String, Integer> domains = Maps.newConcurrentMap();
    private final static String HTTP = "http://";
    private final static String HTTPS = "https://";
    private final String apikey = "blu3sh0rtzAzaSyBOaGpzKvmplyNlRb2fnYFfTOnw5HY9B";
    @Autowired
    private ShortenerService shortenerService;

    public synchronized static void useDomain(String key) {
        Integer counter = domains.get(key);
        if (counter != null) {
            log.debug("Domain: " + key + ", Used: " + (counter + 1));
            domains.put(key, counter + 1);
        }
    }

    public synchronized static String getDomain() {
        final Map<String, Object> temp = new HashMap<>();
        temp.put("minTimes", Integer.MAX_VALUE);
        temp.put("domain", "");
        domains.keySet().forEach(key -> {
            int counter = domains.get(key);
            if (counter < Integer.valueOf(temp.get("minTimes").toString())) {
                temp.put("minTimes", counter);
                temp.put("domain", key);
            }
        });

        return temp.get("domain").toString();
    }

    @PostConstruct
    void init() {
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(ShortenerApplicationService.class.getClassLoader().getResourceAsStream("domains")));
            Stream<String> lines = buffer.lines();
            lines.forEach(line -> domains.put(line, 0));
            lines.close();
        } catch (Exception e) {
            domains.put("luzy.co", 0);
            SlackHelper.sendException(SlackChannel.Exception, "[ERROR] Error while loading domain", ExceptionUtils.getStackTrace(e));
        }
    }

    public ShortenResponse shorten(Long userId, String url) {
        url = url.trim();

        if (StringUtils.isEmpty(url)) {
            throw new ShortenerException("Bad request", HttpStatus.BAD_REQUEST);
        }

        if (url.indexOf(HTTP) != 0 && url.indexOf(HTTPS) != 0) { // Auto add http
            url = HTTP + url;
        }

        ShortenUrlModal shortenUrl = shortenerService.shorten(userId, url);
        if (shortenUrl == null)
            throw new ShortenerException("Can not shorten url", HttpStatus.NOT_FOUND);

        String domain = getDomain();
        String shortUrl = "https://" + domain + "/" + shortenUrl.getCode();
        useDomain(domain);
        log.info("Shorten [" + url + "] to [" + shortUrl + "]");
        return new ShortenResponse(shortenUrl.getId(), shortUrl, new Date(shortenUrl.getCreationDate().getValue()), true, "Shorten url is successful");
    }

    public String getRedirectionUrl(Long userId, String code, Map<String, String> requestInfo) {
        return shortenerService.getRedirectionUrl(userId, code, requestInfo);
    }

    public ShortenStats getStats(Long userId, String shortUrl) {
        ShortenUrlModal shortenUrl = shortenerService.getStats(userId, urlToCode(shortUrl));
        return new ShortenStats("OK", shortenUrl.getCreationDate().toString(), shortUrl, shortenUrl.getUrl(), shortenUrl.getHits());
    }

    public StatsBatchResponse getHit(Long userId, List<String> batch) {
        Long clicked = 0L;
        Long totalClick = 0L;
        final ExecutorService threadPool = Executors.newFixedThreadPool(50, new ThreadFactoryBuilder().setNameFormat("[userId=" + userId + "]-getHit-[" + Math.random() + "]").build());

        try {
            final List<Callable<Long>> callables = new ArrayList<>();
            for (String shortUrl : batch) {
                callables.add(() -> shortenerService.getHit(userId, urlToCode(shortUrl)));
            }

            for (Future<Long> result : threadPool.invokeAll(callables)) {
                Long hit = result.get();
                if (hit == null) {
                    hit = 0L;
                }
                if (hit > 0) {
                    clicked++;
                }
                totalClick += hit;
            }
        } catch (Exception e) {
            SlackHelper.sendException(SlackChannel.Exception, "[ERROR] Error occurs while getting hit.", ExceptionUtils.getStackTrace(e));
        } finally {
            threadPool.shutdown();
        }
        return new StatsBatchResponse(clicked, totalClick);
    }

    public DownloadStatBatch download(Long userId, DownloadStatBatch batch) {
        final ExecutorService threadPool = Executors.newFixedThreadPool(50, new ThreadFactoryBuilder().setNameFormat("[userId=" + userId + "]-download-[" + Math.random() + "]").build());

        try {
            final List<Callable<Void>> callables = new ArrayList<>();
            batch.getBatch().forEach(stat -> callables.add(() -> {
                stat.setClick(shortenerService.getHit(userId, urlToCode(stat.getShortUrl())));
                return null;
            }));

            threadPool.invokeAll(callables);
        } catch (Exception e) {
            SlackHelper.sendException(SlackChannel.Exception, "[ERROR] Error occurs while downloading stat.", ExceptionUtils.getStackTrace(e));
        } finally {
            threadPool.shutdown();
        }
        log.info("DownloadStatBatch: " + new Gson().toJson(batch));
        return batch;
    }

    private String urlToCode(String shortUrl) {
        String trimUrl = shortUrl.trim();
        return trimUrl.substring(trimUrl.lastIndexOf("/") + 1);
    }

    public void validateKey(String apikey) {
        if (!this.apikey.equals(apikey)) {
            throw new ShortenerException("Invalid api key", HttpStatus.UNAUTHORIZED);
        }
    }
}
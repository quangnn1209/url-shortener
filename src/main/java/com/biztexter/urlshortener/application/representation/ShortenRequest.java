package com.biztexter.urlshortener.application.representation;

/**
 * Created by quang on 17/10/08.
 */
public class ShortenRequest {
    private String longUrl;

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
}

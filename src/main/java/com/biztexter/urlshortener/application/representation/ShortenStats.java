package com.biztexter.urlshortener.application.representation;

/**
 * Created by quang on 17/10/08.
 */
public class ShortenStats {
    private String status;
    private String created;
    private String longUrl;
    private String shortUrl;
    private UrlShortenerAnalytics analytics;

    public ShortenStats(String status, String created, String shortUrl, String longUrl, Long hits){
        this.status=status;
        this.created=created;
        this.shortUrl = shortUrl;
        this.longUrl = longUrl;
        UrlShortenerClickCount clickCount = new UrlShortenerClickCount();
        clickCount.setLongUrlClicks(hits);
        clickCount.setShortUrlClicks(hits);
        this.analytics = new UrlShortenerAnalytics();
        analytics.setAllTime(clickCount);
        analytics.setDay(clickCount);
        analytics.setMonth(clickCount);
        analytics.setTwoHours(clickCount);
        analytics.setWeek(clickCount);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public UrlShortenerAnalytics getAnalytics() {
        return analytics;
    }

    public void setAnalytics(UrlShortenerAnalytics analytics) {
        this.analytics = analytics;
    }

    class UrlShortenerAnalytics {
        private UrlShortenerClickCount allTime;
        private UrlShortenerClickCount month;
        private UrlShortenerClickCount week;
        private UrlShortenerClickCount day;
        private UrlShortenerClickCount twoHours;

        public UrlShortenerClickCount getAllTime() {
            return allTime;
        }

        public void setAllTime(UrlShortenerClickCount allTime) {
            this.allTime = allTime;
        }

        public UrlShortenerClickCount getMonth() {
            return month;
        }

        public void setMonth(UrlShortenerClickCount month) {
            this.month = month;
        }

        public UrlShortenerClickCount getWeek() {
            return week;
        }

        public void setWeek(UrlShortenerClickCount week) {
            this.week = week;
        }

        public UrlShortenerClickCount getDay() {
            return day;
        }

        public void setDay(UrlShortenerClickCount day) {
            this.day = day;
        }

        public UrlShortenerClickCount getTwoHours() {
            return twoHours;
        }

        public void setTwoHours(UrlShortenerClickCount twoHours) {
            this.twoHours = twoHours;
        }
    }

    class UrlShortenerClickCount {
        private long shortUrlClicks;
        private long longUrlClicks;

        public long getShortUrlClicks() {
            return shortUrlClicks;
        }

        public void setShortUrlClicks(long shortUrlClicks) {
            this.shortUrlClicks = shortUrlClicks;
        }

        public long getLongUrlClicks() {
            return longUrlClicks;
        }

        public void setLongUrlClicks(long longUrlClicks) {
            this.longUrlClicks = longUrlClicks;
        }
    }
}

package com.biztexter.urlshortener.application.representation;


import java.util.Date;

/**
 * Created by Quang on 24/10/17.
 */
public class ShortenResponse {

    private Long urlId;

    private String shortUrl;

    private String id;

    private Date createdAt;

    private Boolean status;

    private String description;

    public ShortenResponse(Long urlId, String shortUrl, Date createdAt, Boolean status, String description) {
        setUrlId(urlId);
        setShortUrl(shortUrl);
        setId(shortUrl);
        setCreatedAt(createdAt);
        setStatus(status);
        setDescription(description);
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public Long getUrlId() {
        return urlId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    private void setUrlId(Long urlId) {
        this.urlId = urlId;
    }

    private void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    private void setStatus(Boolean status) {
        this.status = status;
    }

    private void setDescription(String description) {
        this.description = description;
    }
}
package com.biztexter.urlshortener.application.representation;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by quang on 17/10/14.
 */
@Getter
@Setter
public class DownloadStatBatch {
    private List<DownloadStat> batch;
}


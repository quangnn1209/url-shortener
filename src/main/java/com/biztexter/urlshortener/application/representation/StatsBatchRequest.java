package com.biztexter.urlshortener.application.representation;

import java.util.List;

/**
 * Created by quang on 17/10/14.
 */
public class StatsBatchRequest {
    private List<String> shortUrls;

    public List<String> getShortUrls() {
        return shortUrls;
    }

    public void setShortUrls(List<String> shortUrls) {
        this.shortUrls = shortUrls;
    }
}

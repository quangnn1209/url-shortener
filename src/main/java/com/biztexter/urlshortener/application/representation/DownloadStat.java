package com.biztexter.urlshortener.application.representation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DownloadStat {
    private long id;
    private String shortUrl;
    private long click;
    private String phone;
    private String created;
}
package com.biztexter.urlshortener.application.representation;

/**
 * Created by quang on 17/10/14.
 */
public class StatsBatchResponse {
    private Long urlClicked;
    private Long totalClick;

    public StatsBatchResponse(Long urlClicked, Long totalClick){
        this.urlClicked = urlClicked;
        this.totalClick = totalClick;
    }

    public Long getUrlClicked() {
        return urlClicked;
    }

    public void setUrlClicked(Long urlClicked) {
        this.urlClicked = urlClicked;
    }

    public Long getTotalClick() {
        return totalClick;
    }

    public void setTotalClick(Long totalClick) {
        this.totalClick = totalClick;
    }
}

node {
    try {
        def project = 'causal-prism-200821'
        def appName = 'urlshortener'
        def imageTag = "gcr.io/${project}/${appName}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
        def environment = 'prod'

        notifyBuild('STARTED')

        stage('Checkout') {
            checkout scm
        }

        stage('Sonarqube scan') { sh("sudo ./gradlew clean sonarqube") }

        stage('Build project') {
            sh("sudo ./gradlew clean build -Denvironment=${environment} -x test --refresh-dependencies")
        }

        stage('Build docker image') {
            sh("sudo gcloud config set account 814190923224-compute@developer.gserviceaccount.com")
            sh("sudo docker build -t ${imageTag} .")
        }

        stage('Push image to registry') {
            sh("sudo gcloud docker -- push ${imageTag}")
        }

        stage('Deploy Application') {
            sh("sudo gcloud container clusters get-credentials urlshortener --zone us-east1-b --project causal-prism-200821")
            sh("sudo sed -i.bak 's#gcr.io/causal-prism-200821/urlshortener#${imageTag}#' k8s/production/production.yaml")
            sh("sudo kubectl --namespace=production apply -f k8s/production/production.yaml")
            sh("sudo kubectl --namespace=production apply -f k8s/production/services/service.yaml")
            sh("sudo kubectl --namespace=production apply -f k8s/production/services/lb/ingress.yaml")
        }
  } catch (e) {
      currentBuild.result = "FAILED"
      throw e
    } finally {
      notifyBuild(currentBuild.result)
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESSFUL'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "URL SHORTENER SERVICE :bee: ${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' :bee: "
    def iconSlack = ':nyancat: :nyancat: :nyancat: :nyancat: :nyancat: '

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
        iconSlack = ':aawyeah: :aawyeah: :aawyeah: :aawyeah: :aawyeah:'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
        iconSlack = ':facepalm: :facepalm: :facepalm: :facepalm: :facepalm:'
    }

    def slackURL = "https://hooks.slack.com/services/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"
    def jenkinsIcon = "https://wiki.jenkins-ci.org/download/attachments/327683/JENKINS?version=1&modificationDate=1302750804000"

    sh  """#!/bin/bash
        PAY_LOAD="{\\\"text\\\":\\\"${subject}\\\",\\\"channel\\\":\\\"#1equiitext-cloud-code\\\",\\\"username\\\":\\\"jenkins\\\",\\\"icon_url\\\":\\\"${jenkinsIcon}\\\",\\\"attachments\\\":[{\\\"color\\\":\\\"${colorCode}\\\",\\\"text\\\":\\\"Branch: ${env.BRANCH_NAME} ${iconSlack}\\\"}]}"
        curl -X POST -H "Content-Type: application/json" -d "\${PAY_LOAD}" ${slackURL}
        """
}
#!/usr/bin/env bash
./gradlew clean build -Denvironment=$1 -x test --refresh-dependencies
sudo docker build -t us.gcr.io/affable-berm-118523/url-shortener:${1:-dev} .
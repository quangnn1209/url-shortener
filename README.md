#To shorten url
POST servername
{"longUrl":"your url"}

#To get stats of one url
GET servername/stat?shortUrl=

#To get stats of many urls
POST servername/stat/
{
	"shortUrls":["https://tezi.us/uM7y00IDs", "uM8yxZyOy"]
}

#To redirect url
GET domain/code
Ex: https://seqi.us/z9HuHY924

#Deploy
pkill java
nohup java -jar release/urlshortener-1.0-prod.jar > prod.log &

#Setting ports (for prod)
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8443

#Development
If you want to make change to GAE module, please do the following steps:
- Finish development on console module in cloud-web-app repo
- Push the change to google app engine (./gradlew appengineUpdate -Denvironment=dev). 
The change will also be published to artifactory 
- Refresh dependencies (or build with ./gradlew clean build -Denvironment=dev --refresh-dependencies)

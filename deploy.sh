#!/usr/bin/env bash
./gradlew clean build -Denvironment=$1 -x test # alpha, prod
cp build/libs/urlshortener-1.0-$1.jar release/